#include <iostream>
#include "Recurse.h"
#include "Supp.h"

int main(int argc, char**argv){
	
	int kth = 2;
	
	LinkedList<int> theList;
	
	theList.insert(22);
	theList.insert(55);
	theList.insert(88);
	theList.insert(102);
	theList.insert(10);
	
	std::cout << "Deleting the " << kth << "-to-last element" << std::endl;
	std::cout << "Initial List: " << theList << std::endl;
	
	delKthElem(&theList, &kth);
	
	std::cout << "Final List  : " << theList << std::endl;
	
	return 0;
	
}